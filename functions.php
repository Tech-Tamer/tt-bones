<?php

if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Initialize Genesis
require_once( get_template_directory() . '/lib/init.php' );

// Child theme definitions
define( 'CHILD_THEME_NAME', 'Custom based on TT Bones' );
define( 'CHILD_THEME_URL', 'http://tech-tamer.com/' );
define( 'CHILD_THEME_VERSION', '1.0' );

// Developer Tools
require_once( CHILD_DIR . '/includes/developer-tools.php' );		// DO NOT USE THESE ON A LIVE SITE

// Genesis
require_once( CHILD_DIR . '/includes/genesis.php' );				// Customizations to Genesis-specific functions

// Tech-Tamer
//require_once( CHILD_DIR . '/includes/tt.php' );				// TT Added / TT Customizations

//Google Web Font Loader, if using
require_once( CHILD_DIR . '/includes/font-loader.php' );				// TT Added keep font-loader and lots of suggested fonts in sep file

// Admin
require_once( CHILD_DIR . '/includes/admin/admin-functions.php' );	// Customization to admin functionality
require_once( CHILD_DIR . '/includes/admin/admin-views.php' );		// Customizations to the admin area display
require_once( CHILD_DIR . '/includes/admin/admin-branding.php' );	// Admin view customizations that specifically involve branding
require_once( CHILD_DIR . '/includes/admin/admin-options.php' );	// For adding/editing theme options to Genesis

// Structure (corresponds to Genesis's lib/structure)
require_once( CHILD_DIR . '/includes/structure/archive.php' );
require_once( CHILD_DIR . '/includes/structure/comments.php' );
require_once( CHILD_DIR . '/includes/structure/footer.php' );
require_once( CHILD_DIR . '/includes/structure/header.php' );
require_once( CHILD_DIR . '/includes/structure/layout.php' );
require_once( CHILD_DIR . '/includes/structure/loops.php' );
require_once( CHILD_DIR . '/includes/structure/menu.php' );
require_once( CHILD_DIR . '/includes/structure/post.php' );
require_once( CHILD_DIR . '/includes/structure/search.php' );
require_once( CHILD_DIR . '/includes/structure/sidebar.php' );

// Shame
require_once( CHILD_DIR . '/includes/shame.php' );					// For new code snippets that haven't been sorted and commented yet
