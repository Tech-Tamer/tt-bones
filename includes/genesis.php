<?php

if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/* **************************
 * TOC - [DIS] = disabled until specifically ENABLED
 *
 * Force HTML5
 * Adds <meta> tags for mobile responsiveness.
 * [DIS] Add support for custom backgrounds
 * [DIS] Add support for a custom header
 * [DIS] Add Genesis post format support
 *  Add Genesis footer widget areas [3]
 * [DIS] Add Genesis theme color scheme selection theme option
 * Declare WooCommerce support, using Genesis Connect for WooCommerce
 * [DIS] Unregister default Genesis layouts
 * [DIS] Unregister default Genesis sidebars
 * [DIS] Unregister default Genesis menus and add your own
 * [SOME] Disable some or all of the default Genesis widgets.
 * Remove the Genesis 'Layout Settings' meta box for posts and/or pages.
 * Remove the Genesis 'Scripts' meta box for posts and/or pages.
 * [DIS] Remove or alter the post meta info in header, footer
 * [DIS] Filter the genesis_seo_site_title function to use an image for the logo instead of a background image
 * [TT Added] Add custom shortcodes - for year (use in copyright) and login/out
 ************************* */


/**
 * Force HTML5
 * See: http://www.briangardner.com/code/add-html5-markup/
 * @since 2.0.0
 */
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

/**
 * Adds <meta> tags for mobile responsiveness.
 * See: http://www.briangardner.com/code/add-viewport-meta-tag/
 * @since 2.0.0
 */
add_theme_support( 'genesis-responsive-viewport' );

/**
 * Add support for custom backgrounds
 * @since 2.0.2
 */
// add_theme_support( 'custom-background' );

/**
 * Add support for a custom header
 * @since 2.0.9
 */
// add_theme_support( 'genesis-custom-header', array( 'width' => 960, 'height' => 100 ) );

/**
 * Add Genesis post format support
 *
 * @since 2.0.9
 */
// add_theme_support( 'post-formats', array(
// 	'aside',
// 	'chat',
// 	'gallery',
// 	'image',
// 	'link',
// 	'quote',
// 	'status',
// 	'video',
// 	'audio'
// ));
// add_theme_support( 'genesis-post-format-images' );

/**
 * Add Genesis footer widget areas
 * @since 2.0.1
 */
add_theme_support( 'genesis-footer-widgets', 3 );

/**
 * Add Genesis theme color scheme selection theme option
 * @since 2.0.11
 */
// add_theme_support(
// 	'genesis-style-selector',
// 	array(
// 		'bfg-red' => 'Red',
// 		'bfg-orange' => 'Orange'
// 	)
// );

/**
 * Declare WooCommerce support, using Genesis Connect for WooCommerce
 * See: http://wordpress.org/plugins/genesis-connect-woocommerce/
 * @since 2.0.6
 */
// add_theme_support( 'genesis-connect-woocommerce' );

/**
 * Unregister default Genesis layouts
 *
 * @since 1.x
 */
// genesis_unregister_layout( 'content-sidebar' );
// genesis_unregister_layout( 'sidebar-content' );
// genesis_unregister_layout( 'content-sidebar-sidebar' );
// genesis_unregister_layout( 'sidebar-sidebar-content' );
// genesis_unregister_layout( 'sidebar-content-sidebar' );
// genesis_unregister_layout( 'full-width-content' );

/**
 * Unregister default Genesis sidebars
 *
 * @since 1.x
 */
// unregister_sidebar( 'header-right' );
// unregister_sidebar( 'sidebar-alt' );
// unregister_sidebar( 'sidebar' );

/**
 * Unregister default Genesis menus and add your own
 *
 * @since 2.0.18
 */
// remove_theme_support( 'genesis-menus' );
// add_theme_support(
// 	'genesis-menus',
// 	array(
// 		'primary' => 'Primary Menu',
// 		'secondary' => 'Secondary Menu',
// 	)
// );

add_action( 'widgets_init', 'tt_remove_genesis_widgets', 20 );
/**
 * Disable some or all of the default Genesis widgets.
 *
 * @since 2.0.0
 */
function tt_remove_genesis_widgets() {

	//unregister_widget( 'Genesis_Featured_Page' );									// Featured Page
	unregister_widget( 'Genesis_User_Profile_Widget' );								// User Profile
	//unregister_widget( 'Genesis_Featured_Post' );									// Featured Posts

}

// add_action( 'init', 'tt_remove_layout_meta_boxes' );
/**
 * Remove the Genesis 'Layout Settings' meta box for posts and/or pages.
 * @since 2.0.0
 */
function tt_remove_layout_meta_boxes() {

	remove_post_type_support( 'post', 'genesis-layouts' );							// Posts
	remove_post_type_support( 'page', 'genesis-layouts' );							// Pages

}

add_action( 'init', 'tt_remove_scripts_meta_boxes' );
/**
 * Remove the Genesis 'Scripts' meta box for posts and/or pages.
 * @since 2.0.12
 */
function tt_remove_scripts_meta_boxes() {

	remove_post_type_support( 'post', 'genesis-scripts' );							// Posts
	remove_post_type_support( 'page', 'genesis-scripts' );							// Pages

}

// TT Added

/**
 * Add custom shortcodes - for year (use in copyright) and login/out
 * @link http://justintadlock.com/archives/2011/08/30/adding-a-login-form-to-a-page
 * http://justintadlock.com/archives/2011/08/30/adding-a-login-form-to-a-page
 */
// Use [year] to echo the current year
// Use [loginout] to echo a login or logout link depending on user's status
add_action( 'init', 'tt_add_shortcodes' );
function tt_add_shortcodes() {
	add_shortcode( 'loginout', 'tt_loginout_shortcode' );
	add_shortcode( 'year', 'tt_year_shortcode' );
}
function tt_year_shortcode() {
	$year = date('Y');
	return $year;
}
function tt_loginout_shortcode() {
	if ( is_user_logged_in() ) {
		return '<a href="'.wp_logout_url( get_permalink() ).'" title="Logout">Logout</a>';
	} else {
		return '<a href="'.wp_login_url( get_permalink() ).'" title="Login">Login</a>';
	}
}