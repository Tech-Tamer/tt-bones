<?php

// *** See also Google Font loader in header.php

// Add webfontloader (Google/Typekit collaboration)

// Two options: sync (no FOUT but delays content) and async (content faster, but FOUT)
// async avoids blocking your page while loading the JS. If async, the rest of the page might render
// before the Web Font Loader is loaded and executed, which can cause a Flash of Unstyled Text (FOUT).
//
// If sync, wf-loading class is set on the HTML element as soon as Webfont.load has been called.
// The browser will wait for the script to load before continuing to load the rest of the content, FOUT is avoided.


// FONTS & FONT LOADER
// list of good fonts to try: see eg
// http://hellohappy.org/beautiful-web-type
// http://www.awwwards.com/20-best-web-fonts-from-google-web-fonts-and-font-face.html
// http://www.typewolf.com/open-source-web-fonts


// SANS SERIF
// http://www.google.com/fonts/specimen/PT+Sans+Narrow
// http://www.google.com/fonts/specimen/PT+Sans
// http://www.google.com/fonts/specimen/Lato
// http://www.google.com/fonts/specimen/Open+Sans
// http://www.google.com/fonts/specimen/Cardo
// http://www.google.com/fonts/specimen/Ubuntu
// http://www.google.com/fonts/specimen/Droid+Sans
// http://www.google.com/fonts/specimen/Montserrat
//

// SERIF
// http://www.google.com/fonts/specimen/Vollkorn
// http://www.google.com/fonts/specimen/Gentium+Book+Basic
// http://www.google.com/fonts/specimen/Cardo
// http://www.google.com/fonts/specimen/Merriweather
// http://www.google.com/fonts/specimen/PT+Serif
// http://www.google.com/fonts/specimen/Old+Standard+TT
//
//
//

// DISPLAY
// http://www.google.com/fonts/specimen/Abril+Fatface
// http://www.google.com/fonts/specimen/Gravitas+One
// http://www.google.com/fonts/specimen/Playfair+Display

// SLAB
// http://www.google.com/fonts/specimen/Josefin+Slab
//
//

//MONO
// http://www.google.com/fonts/specimen/Inconsolata
// http://www.google.com/fonts/specimen/PT+Mono
//
//
add_action( 'wp_head', 'tt_webfontloader_sync' );
//add_action( 'wp_head', 'tt_webfontloader_async' );
function tt_webfontloader_sync() {
	?>
	<script src="//ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
	<script>
		WebFont.load({
			google: { families: ['Lato'] },
			// can specify which types - italic (or i); bold (or b or numeric weight); bolditalic (or bi)
			// maybe omit single quotes and use "+" to simplify too
			// google:{ [Cantarell:italic, Droid+Serif:bold]},
			timeout: 2000 // Set the timeout to two seconds; abandon fonts if longer than 2 sec
		});
	</script>
<?php }

// ASYNC
function tt_webfontloader_async() { ?>
	<script>
		WebFontConfig = {
			google: {families: ['Droid Sans', 'Droid Serif:bold']}
			//typekit: { id: 'xxxxxx' }
			//fontdeck: { id: 'xxxxx'}
			//monotype: { projectId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx', version: 12345 }
			// // (version is optional, flushes the CDN cache)
		};
		( function () {
			var
				wf         = document . createElement( 'script' );
			wf . src   = ( 'https:' == document . location . protocol ? 'https' : 'http' ) +
			'://ajax.googleapis.com/ajax/libs/webfont/1.5.6/webfont.js';
			wf . type  = 'text/javascript';
			wf . async = 'true';
			var
				s = document . getElementsByTagName( 'script' )[0];
			s . parentNode . insertBefore( wf, s );
		} )();
	</script>
<?php }